# 백패커 (아이디어스) 사전과제

## 사용한 라이브러리
- react
- storybook
- scss

## 스크립트
`npm run storybook` 으로 localhost:6006에서 결과물 확인 

## 결과물 이미지
![screencapture-localhost-6006-2022-05-15-19_56_04](https://user-images.githubusercontent.com/31813858/168469305-9fdc7062-45bf-446f-920d-139734aa4327.png)
![screencapture-localhost-6006-2022-05-15-19_57_14](https://user-images.githubusercontent.com/31813858/168469333-698105d9-7f85-417e-bd68-7de8de758ce3.png)
