import { FC } from 'react';
import './input.scss';

interface InputProps {
    value: string;
    setValue: (value: string) => void;
    placeholder: string;
    disabled: boolean;
    readOnly: boolean;
}

const Input: FC<InputProps> = ({ value = '', setValue, placeholder, disabled, readOnly }) => {

    return (
        <>
            <div className="input">
                <div className="input-field" style={{ width: value !== '' || !readOnly ? '80%' : '100%' }}>
                    <textarea className={`${disabled ? 'disabled' : ''} ${readOnly ? 'readonly' : ''}`} disabled={disabled} readOnly={readOnly} placeholder={placeholder} value={value} onChange={(e) => setValue(e.target.value)}></textarea>
                    <div className="input-count">
                        {value.length}
                    </div>
                </div>
                {value !== '' || !readOnly ? <button className="input-button" disabled={disabled}>
                    Save
                </button> : null}
            </div>
        </>
    )
}

export default Input;