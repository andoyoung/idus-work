import { FC } from 'react';
import FillStarIcon from './../icons/fillStar.svg';
import EmptyStarIcon from './../icons/emptyStar.svg';
import './card.scss';

interface CardProps {
    title: string;
    text: string;
    rating: number;
    isHorizontal: boolean;
    isShowTitle: boolean;
    isShowText: boolean;
    isShowRating: boolean;
}

const Card: FC<CardProps> = ({ title, text, rating, isHorizontal, isShowTitle, isShowText, isShowRating }) => {

    return (
        <>
            <div className={`card ${isHorizontal ? 'card-horizontal' : ''}`}>
                <div className={`card-image-wrapper ${isHorizontal ? 'card-image-wrapper-horizontal' : ''}`}>
                    <div className="card-image" style={{ backgroundImage: `url("https://www.learningcontainer.com/wp-content/uploads/2020/08/Large-Sample-png-Image-download-for-Testing.png")` }}></div>
                </div>
                <div className="card-content">
                    {isShowTitle ? <div className="card-title">
                        {title}
                    </div> : null}
                    {isShowText ? <div className="card-text">
                        {text}
                    </div> : null}
                    {isShowRating ? <div className="card-rating">
                        {[...new Array(rating)].map((value, index) => <img key={index} src={FillStarIcon} alt="" />)}
                        {[...new Array(5 - rating)].map((value, index) => <img key={index} src={EmptyStarIcon} alt="" />)}
                    </div> : null}
                </div>
            </div>
        </>
    )
}

export default Card;