import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Card from './../components/card/Card';

export default {
  title: 'Component/Card',
  component: Card,
  argTypes: {
    title: {
      control: 'text',
      description: '카드 제목'
    },
    text: {
      control: 'text',
      description: '카드 내용'
    },
    rating: {
      control: {
        type: 'range',
        min: 1,
        max: 5,
        step: 1
      },
      description: '별점 갯수'
    }
  },
} as ComponentMeta<typeof Card>;

const Template: ComponentStory<typeof Card> = (args) => {
    return (
        <>
          <div style={{ width: '100%', display: 'flex', marginBottom: 8 }}>
            <div style={{ width: 400, marginRight: 8 }}>
              <h3>Case1 모든 항목 노출</h3>
              <Card {...args} isShowTitle isShowText isShowRating />
            </div>
            <div style={{ width: 400 }}>
            <h3>Case2 제목 및 텍스트 영역 노출</h3>
              <Card {...args} isShowTitle isShowText />
            </div>
          </div>
          <div style={{ width: '100%', display: 'flex' }}>
            <div style={{ width: 400, marginRight: 8 }}>
              <h3>Case3 제목 및 별점 영역 노출</h3>
              <Card {...args} isShowTitle isShowRating />
            </div>
            <div style={{ width: 600 }}>
              <h3>Case4 세로 카드</h3>
              <Card {...args} isHorizontal isShowTitle isShowText isShowRating />
            </div>
          </div>
        </>
    )
};

export const AllStory = Template.bind({});
AllStory.args = {
  title: 'Card Title',
  text: 'Card Text',
  rating: 1,
};
AllStory.storyName = '전체모습';
