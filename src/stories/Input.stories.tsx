import React, { useState } from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Input from './../components/input/Input';

export default {
  title: 'Component/Input',
  component: Input,
  argTypes: {
    placeholder: { control: 'text' },
    disabled: { control: 'boolean' },
    readOnly: { control: 'boolean' },
    value: { control: 'string' }
  },
} as ComponentMeta<typeof Input>;

const Template: ComponentStory<typeof Input> = (args) => {
    const [value, setValue] = useState<string>(args.value);
    return (
        <>
            <Input {...args} value={value} setValue={setValue} />
        </>
    )
};

export const BasicStory = Template.bind({});
BasicStory.args = {
    placeholder: 'Placeholder',
    disabled: false,
    readOnly: false,
};
BasicStory.storyName = '기본';

export const TypingStory = Template.bind({});
TypingStory.args = {
    placeholder: 'Placeholder',
    disabled: false,
    readOnly: false,
    value: '입력중'
};
TypingStory.storyName = '입력중';

export const DisabledStory = Template.bind({});
DisabledStory.args = {
    placeholder: 'Placeholder',
    disabled: true,
    readOnly: false,
};
DisabledStory.storyName = '입력불가';

export const readOnlyStory = Template.bind({});
readOnlyStory.args = {
    placeholder: 'Placeholder',
    disabled: false,
    readOnly: true,
};
readOnlyStory.storyName = '읽기전용';